package com.example.memo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SparseIntArray textSoundMapping;
    private SparseIntArray pairsMapping;
    private int[] imgViewIds = {
            R.id.imgView1, R.id.imgView2, R.id.imgView3, R.id.imgView4, R.id.imgView5,
            R.id.imgView6, R.id.imgView7, R.id.imgView8, R.id.imgView9, R.id.imgView10,
            R.id.imgView11, R.id.imgView12
    };
    private int[] drawableAnimalsIds = { //tablica obrazków
            R.drawable.cat, R.drawable.dog, R.drawable.elephant, R.drawable.frog,
            R.drawable.bear, R.drawable.cow, R.drawable.tuger, R.drawable.fish, R.drawable.croco,
            R.drawable.giraffe, R.drawable.hippo, R.drawable.pig
    };
    private int[] drawableTextIds = { //obrazki z tekstem
            R.drawable.cat_text, R.drawable.dog_text, R.drawable.elephant_text,
            R.drawable.frog_text, R.drawable.bear_text, R.drawable.cow_text, R.drawable.tiger_text,
            R.drawable.fish_text, R.drawable.croco_text, R.drawable.giraffe_text,
            R.drawable.hip_text, R.drawable.pig_text
    };
    private int[] rawIds = { //dzwiek
            R.raw.cat, R.raw.dog, R.raw.elephant, R.raw.frog, R.raw.bear, R.raw.cow, R.raw.tiger,
            R.raw.fish, R.raw.crocodile, R.raw.giraffe, R.raw.hippo, R.raw.pig
    };
    private ImageView faceUpCardImageView = null; //zmienna przechowujaca uchwyt do odkrytej karty (info który komponent został klikniety)
    private int faceUpCardDrawableId = -1; // zmienna przechowuje informację o id który obrazek jest w komponencie (np że to świnia)
    private int cardsNumber = 6; //ile ma być wyświetlonych kart na planszy
    private int points = 0; //liczba punktów, która zwieksza sie o 1 za kazdym razem gdy odkryjemy pare i zeruje gdy odkryjemy wszystkie
    private Button startButton; // uchwyt do przycisku "start"

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton = (Button) findViewById(R.id.startBtn);

        initializePairsMapping();// metoda łącząca w pary obrazki img z obrazkami z tekstem oraz obrazki z tekstem z dzwiekiem
        setInitialState();//ustawia pocz stan aplikacji

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        });

    }

    private void setInitialState() {
        for(int imgViewId: imgViewIds) {
            ImageView imageView  = findViewById(imgViewId);
            imageView.setVisibility(View.INVISIBLE);
            imageView.setEnabled(false);
        }
        startButton.setEnabled(true);
        points = 0;
        cardsNumber = 6;
    }

    private void startGame() {
        points = 0;
        startButton.setEnabled(false);//przestaje być klikalny jak rozpoczniemy gre
        List<Integer> shuffledList = getShuffledList(cardsNumber); //shuffledList (pomieszana lista) o rozmiarze takim jak cardsNumber
        initializeBoard(shuffledList, cardsNumber);
    }

    private void initializePairsMapping() {
        // metoda inicjalizuje zmienne umozliwiajce znajdowanie par oraz odtwarzanie odpowiednich dzwiekow dla okreslonego tekstu
        pairsMapping = new SparseIntArray();
        textSoundMapping = new SparseIntArray();
        for (int i = 0; i < drawableAnimalsIds.length; i++) {
            pairsMapping.append(drawableAnimalsIds[i], drawableTextIds[i]); // dodajemy do mapy id obrazka i id tekstu tego obrazka
            textSoundMapping.append(drawableTextIds[i], rawIds[i]); //klucz - id tekstu i wartosc - id dzwieku
            // workaround
            pairsMapping.append(drawableTextIds[i], drawableAnimalsIds[i]); // dodanie po to żeby można było również znajdować parę po wartości
        }
    }

    private List<Integer> getImageIds(int number) {
        List<Integer> imageIds = new ArrayList<>();
        List<Integer> shuffledList = getShuffledList(number);
        for (int i = 0; i < number / 2; i++) { // przez 2 bo są pary
            imageIds.add(drawableAnimalsIds[shuffledList.get(i)]); // pobieramy numer wygenerowany z sl i obrazek o takim id pobieramy z listy obrazkow
            imageIds.add(drawableTextIds[shuffledList.get(i)]); // pobieranmy tekst o takim samym id co obrazek
        }
        return imageIds;
    }

    private AlertDialog getAlertDialog(String text) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(text);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK!",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cardsNumber = cardsNumber == 6 ? 12 : 6;
                        startGame();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "Anuluj",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        setInitialState();
                        dialog.cancel();
                    }
                });
        return builder1.create();
    }

    private void initializeBoard(final List<Integer> shuffledList, int size) {
        final List<Integer> imageIds = getImageIds(size);
        for (int i = 0; i < size; i++) {
            // setOnClickListener wykorzystuje klase zagniezdzona, do ktorej nie mozemy przekazac
            // zadnych argumentow, musimy stworzyc obejscie poprzez zmienne tymczasowe
            // ktore beda typu final
            final int workAroundIndex = i;
            final ImageView imageView  = findViewById(imgViewIds[i]);
            imageView.setImageResource(R.drawable.blue);
            imageView.setVisibility(View.VISIBLE);
            imageView.setEnabled(true);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int randomImgId = imageIds.get(shuffledList.get(workAroundIndex));
                    imageView.setImageResource(randomImgId);  // w momencie klikniecia na niebieski obrazek ustaw losowo wybrany obrazek z listy imageIds
                    int soundId = textSoundMapping.get(randomImgId);  // sprawdzam czy dla danego obrazka jest do odtworzenia jakis dzwiek
                    if (soundId != 0) { // soundId jest rozne od 0, jesli z danym obrazkiem jest powiazany jakis dzwiek
                        playResource(soundId);
                    }
                    // poszukiwanie par
                    if (faceUpCardImageView == null) { // brak odkrytej karty
                        faceUpCardImageView = imageView; // zapamietujemy komponent odkrytej karty
                        faceUpCardDrawableId = randomImgId; // oraz id przypisanego do niej obrazu
                    } else { // jest już odkryta karta
                        Handler handler = new Handler(); // opoznienie ukrycia kart
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (pairsMapping.get(faceUpCardDrawableId) == randomImgId) { // jesli mamy pare
                                    faceUpCardImageView.setVisibility(View.INVISIBLE);
                                    faceUpCardImageView.setClickable(false);
                                    imageView.setVisibility(View.INVISIBLE);
                                    imageView.setClickable(false);
                                    if (++points >= cardsNumber / 2) {
                                        AlertDialog ad = getAlertDialog(
                                                cardsNumber == 6
                                                        ? "Gratulacje, przechodzisz na 2 poziom!"
                                                        : "Wygrales! Zaczynamy od poczatku?"
                                        );
                                        ad.show();
                                    }
                                } else {
                                    faceUpCardImageView.setImageResource(R.drawable.blue);
                                    imageView.setImageResource(R.drawable.blue);
                                }
                                faceUpCardImageView = null;
                                faceUpCardDrawableId = -1;
                            }
                        }, 1000); // karty ukryja sie po 1s od ostatniego ruchu

                    }
                }
            });
        }
    }

    private void playResource(int resourceId) { //przekazujemy tutaj id dzwieku do odtworzenia
        MediaPlayer mp = MediaPlayer.create(this, resourceId);
        mp.start(); //odtwarza dzwiek
        while(mp.isPlaying()); // pusta petla - czekamy az dzwiek przestanie byc odtwarzany
        mp.release(); // zwalniamy zasob (plik dzwiekowy)
    }

    private List<Integer> getShuffledList(int size) {
        /* Metoda tworząca listę typu int od 0 do size, a nastepnie mieszajaca jej zawartosc */
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            list.add(i);
        }
        Collections.shuffle(list);
        return list;
    }

}
